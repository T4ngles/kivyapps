from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput

class LoginScreen(GridLayout):
    def __init__(self,**kwargs):
        super(LoginScreen, self).__init__(**kwargs)
        self.cols = 2

        self.add_widget(Label(text="Search:"))
        self.search = TextInput(multiline=False)
        self.add_widget(self.search)

        self.add_widget(Label(text="Number of pages:"))
        self.pageNo = TextInput(multiline=False, password=True)
        self.add_widget(self.pageNo)

        self.add_widget(Label(text="short/long/0:"))
        self.length = TextInput(multiline=False)
        self.add_widget(self.length)

class SimpleKivy(App):
    def build(self):
        return LoginScreen()

if __name__ == "__main__":
    SimpleKivy().run()

