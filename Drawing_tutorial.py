import kivy
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.graphics import Line
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition

kivy.require("1.9.0")

class MainScreen(Screen):
	pass

class AnotherScreen(Screen):
	pass

class ScreenManagement(ScreenManager):
	pass

class Painter(Widget):
	def on_touch_down(self,touch):
		print(touch)
		with self.canvas:
			touch.ud["line"] = Line(points=(touch.x,touch.y))

	def on_touch_move(self,touch):
		print(touch)
		touch.ud["line"].points += (touch.x,touch.y)

	def on_touch_up(self,touch):
		print("REALESED!",touch)


class LoginScreen(GridLayout):
	def __init__(self, **kwargs):
		super(LoginScreen,self).__init__(**kwargs)
		self.cols = 2

		self.add_widget(Label(text="Username:"))
		self.username = TextInput(multiline=False)
		self.add_widget(self.username)

		self.add_widget(Label(text="Password:"))
		self.password = TextInput(multiline=False, password=True)
		self.add_widget(self.password)

		self.add_widget(Label(text="Captcha:"))
		self.captcha = TextInput(multiline=False)
		self.add_widget(self.captcha)

presentation = Builder.load_file("Drawing_tutorial.kv")

class TestApp(App):
    def build(self):
    	return presentation
        #return TouchInput()
        #return LoginScreen()
        #return Label(text="World Hello!")
        #return Button(text='Hello World')

if __name__ == "__main__":
	TestApp().run()


